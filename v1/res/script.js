resource = {
}

chatMonitor = {
	handle: null,
	interval: 900000, // 15 minutes
	start: function () {
		chat.check()
		handle = setTimeout(this.onTriggered.bind(this), this.interval)
	},
	stop: function () {
		clearTimeout(handle)
	},
	onTriggered: function () {
		chat.check()
	},
}

chat = {
	config: null,
	backendResponseTexts: null,
	backends : [
		'https://stiag.tilde.team/v1/chat/status.php?rnd={rnd}&group=codeberg',
		//'https://stiag.tilde.team/v1/chat/status.php?rnd={rnd}',
	],
	init: function () {
		this.backendResponseTexts = {}
	},
	onBackendResponse: function (backend, xhr, event) {
		//console.log(xhr) //, event)
		//console.log(xhr.status, xhr.responseText)
		var responseText = xhr.responseText
		this.backendResponseTexts[backend] = responseText
		if (responseText.length > 0) {
			var response = JSON.parse(responseText)
			if (response != 0) {
				this.config = response
				this.onAvailable()
			}
		}
	},
	checkBackend: function (backend) {
		var chat = this
		var type = 'GET'
		var url = backend
		var xhr = new XMLHttpRequest()
		xhr.open(type, url, true)
		xhr.onreadystatechange = function(event) {
			if (xhr.readyState == 4) {
				chat.onBackendResponse(backend, xhr, event)
			}
		}
		xhr.send()
	},
	check: function () {
		// TODO: all timeouted or response == 0 => chat goes offline
		// TODO: safe guard to not trigger more requests if previous not finished yet
		var rnd = Date.now()
		for (let backend of this.backends) {
			this.checkBackend(backend.replaceAll('{rnd}', rnd))
		}
	},
	onAvailable: function () {
		if (! this.config) {
			return
		}
		document.body.classList.add('chat-available')
		for (let element of document.getElementsByClassName('chat-link')) {
			element.setAttribute('href', this.config.web_ui_url)
		}
	},
}

scrollDownAnimation = {
	duration : 300,
	events : ['scroll', 'mousedown', 'touchstart', 'keydown'],
	init : function () {
		this.top = {}
		this.onEvent = this._onEvent.bind(this)
		this.step = this._step.bind(this)
		this.inited = true
	},
	_onEvent : function (event) {
		var top = this.getScroll()
		if (! this.top.expected[top]) {
			this.stop()
		} else {
			delete this.top.expected[top]
		}
	},
	getScroll : function () {
		return document.scrollingElement.scrollTop
	},
	setScroll : function (top) {
		document.scrollingElement.scroll(document.scrollingElement.scrollLeft, top)
	},
	getScrollTarget  :function () {
		return (
			document.getElementById('section-current-tasks').getBoundingClientRect().y
			+ document.scrollingElement.scrollTop
			- 20
		)
	},
	start : function () {
		if (this.animation) {
			this.animation.unschedule()
		}
		if (! this.inited) this.init()
		this.top.start = this.getScroll()
		this.top.end = this.getScrollTarget()
		this.top.difference = this.top.end - this.top.start
		this.top.expected = {}
		this.top.expected[this.top.start] = 1
		this.top.expected[this.top.end] = 1
		this.animation = SVG()
			.animate({
				duration: this.duration,
			})
			.during(this.step)
		for (var event of this.events) {
			window.addEventListener(event, this.onEvent)
		}
	},
	stop : function () {
		if (this.animation) {
			this.animation.unschedule()
			this.animation = null
		}
		for (var event of this.events) {
			window.removeEventListener(event, this.onEvent)
		}
	},
	_step : function (progress) {
		var easing = SVG.easing['<>']
		var top = Math.round(this.top.start + easing(progress) * this.top.difference)
		this.top.expected[top] = 1
		this.setScroll(top)
	},
}

function bystandersPopupClick() {
	var bystandersPopup = document.getElementById('bystanders-popup')
	if (bystandersPopup.classList.contains('bystanders-popup-phrase1')) {
		bystandersPopup.classList.remove('bystanders-popup-phrase1')
		bystandersPopup.classList.remove('bystanders-popup-exclamation')
		var tridentSpacerBottom = document.getElementById('trident-spacer-bottom');
		scrollDownAnimation.start()
	} else if (bystandersPopup.classList.contains('bystanders-popup-exclamation')) {
		bystandersPopup.classList.add('bystanders-popup-phrase1')
	} else {
		bystandersPopup.classList.add('bystanders-popup-exclamation')
		setTimeout(function () {
			bystandersPopup.classList.add('bystanders-popup-phrase1')
		}, 1800)
	}
}

function onLoad () {
	chat.init()
	chatMonitor.start()
}

window.addEventListener('load', onLoad)